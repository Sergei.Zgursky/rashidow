document.querySelectorAll('.js-svg-load').forEach((el)=>{
  var $this = el
  var ajax = new XMLHttpRequest();
  var svgSrc = $this.getAttribute('data-svg')
  
  ajax.open("GET", svgSrc, true);
  ajax.send();
  ajax.onload = function(e) {
      $this.innerHTML = ajax.responseText;
  }  
  setTimeout(()=>{
    $this.classList.add('_ok')
  },500)
})
