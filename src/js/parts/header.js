window.addEventListener('scroll', (e) => {
    if (window.scrollY) {
        document.querySelector('.header').classList.add('_fix')
    } else {
        document.querySelector('.header').classList.remove('_fix')
    }
})