if($('.js-interview-load').length) {
    let numberLoad = 0
    $(document).on('scroll', function(){
        $loadbox('.js-click-load-interview', loadInterview)
    })
    $(document).on('click', '.js-click-load-interview', function(){
        loadInterview()
        return false
    })
    function loadInterview(){
        if($(window).scrollTop() + $(window).height() >= $('.js-interview-load').height() && !$('.js-interview-load').hasClass('stop')) {
            var listResult
            var jsonResult = new XMLHttpRequest();
            var requestSrc =  $('.js-interview-load').attr('data-json');
            jsonResult.open("GET", requestSrc);
            jsonResult.responseType = 'json';
            jsonResult.send();
            jsonResult.onload = function(e) {
                listResult = jsonResult.response
                var list = listResult.interview
                if(list.length < numberLoad){
                    $('.js-interview-load').addClass('stop')
                }
                numberLoad += 3
                $.each(list, function(key, val) {
                    if(key < numberLoad && key >= numberLoad - 3) {
                        $('.interview__item').last().after(`
                            <article class="interview__item  animated animated--type-2">
                                <a href="${val.interviewLink}" class="interview__img-box">
                                    <img class="interview__img" src="${val.interviewImg}">
                                </a>
                                <div class="interview__desc">
                                    <div class="interview__time">
                                        <span>${val.interviewTime}</span>
                                        <span>${val.interviewDate}</span>
                                    </div>
                                    <a href="${val.interviewLink}">${val.interviewTitle}</a>
                                    <p>${val.interviewText}</p>
                                </div>
                            </article>
                        `)
                    }
                })
            } 
        }
    }
}
if($('.js-events-load').length) {
    let numberLoad = 0
    $(document).on('scroll', function(){
        $loadbox('.js-click-load-event', loadEvents)
    })
    $(document).on('click', '.js-click-load-event', function(){
        loadEvents()
        return false
    })
    function loadEvents(){
        if($(window).scrollTop() + $(window).height() >= $('.js-events-load').height() && !$('.js-events-load').hasClass('stop')) {
            var listResult
            var jsonResult = new XMLHttpRequest();
            var requestSrc =  $('.js-events-load').attr('data-json');
            jsonResult.open("GET", requestSrc);
            jsonResult.responseType = 'json';
            jsonResult.send();
            jsonResult.onload = function(e) {
                listResult = jsonResult.response
                var list = listResult.events
                if(list.length < numberLoad){
                    $('.js-events-load').addClass('stop')
                }
                numberLoad += 3
                $.each(list, function(key, val) {
                    if(key < numberLoad && key >= numberLoad - 3) {
                        $('.interview__item').last().after(`
                            <article class="interview__item  animated animated--type-2">
                                <a href="${val.eventsLink}" class="interview__img-box">
                                    <img class="interview__img" src="${val.eventsImg}">
                                </a>
                                <div class="interview__desc">
                                    <div class="interview__time interview__time--type-2">
                                        <span>${val.eventsDate}</span>
                                    </div>
                                    <a href="${val.eventsLink}">${val.eventsTitle}</a>
                                    <p>${val.eventsText}</p>
                                </div>
                            </article>
                        `)
                    }
                })
            } 
        }
    }
}

if($('.js-article-load').length) {
    let numberLoad = 0
    $(document).on('scroll', function(){
        $loadbox('.js-click-load-article', loadArticle)
    })
    $(document).on('click', '.js-click-load-article', function(){
        loadArticle()
        return false
    })
    function loadArticle(){
        if($(window).scrollTop() + $(window).height() >= $('.js-article-load').height() && !$('.js-article-load').hasClass('stop')) {
            var listResult
            var jsonResult = new XMLHttpRequest();
            var requestSrc =  $('.js-article-load').attr('data-json');
            jsonResult.open("GET", requestSrc);
            jsonResult.responseType = 'json';
            jsonResult.send();
            jsonResult.onload = function(e) {
                listResult = jsonResult.response
                var list = listResult.article
                let filter = sessionStorage.getItem('filter')
                if(list.length < numberLoad){
                    $('.js-article-load').addClass('stop')
                }
                numberLoad += 3
                $.each(list, function(key, val) {
                    if(key < numberLoad && key >= numberLoad - 3) {
                        var filterHide
                        if(filter != 'all' && val.articleFilter != filter){
                            filterHide = '_hide'
                        }
                        if(val.articleImg != undefined ){
                            $('.interview__item').last().after(`
                                <article class="interview__item  animated animated--type-2 ${filterHide}" data-filter="${val.articleFilter}">
                                    <a href="${val.articleLink}" class="interview__img-box">
                                        <img class="interview__img" src="${val.articleImg}">
                                    </a>
                                    <div class="interview__desc">
                                        <div class="interview__time">
                                            <span>${val.articleTime}</span>
                                            <span>${val.articleDate}</span>
                                        </div>
                                        <a href="${val.articleLink}">${val.articleTitle}</a>
                                        <p>${val.articleText}</p>
                                    </div>
                                </article>
                            `)
                        }else {
                            $('.interview__item').last().after(`
                                <article class="interview__item interview__item--big  animated animated--type-2 ${filterHide}" data-filter="${val.articleFilter}">
                                    <div class="interview__desc">
                                        <div class="interview__time">
                                            <span>${val.articleTime}</span>
                                            <span>${val.articleDate}</span>
                                        </div>
                                        <a href="${val.articleLink}">${val.articleTitle}</a>
                                        <p>${val.articleText}</p>
                                    </div>
                                </article>
                            `)
                        }
                    }
                })
            } 
        }
    }
}

if($('.js-gallery-page-load').length) {
    let numberLoad = 0
    $(document).on('scroll', function(){
        $loadbox('.js-click-load-gallery-page', loadEvents)
    })
    $(document).on('click', '.js-click-load-gallery-page', function(){
        loadEvents()
        return false
    })
    function loadEvents(){
        if($(window).scrollTop() + $(window).height() >= $('.js-gallery-page-load').height() && !$('.js-gallery-page-load').hasClass('stop')) {
            var listResult
            var jsonResult = new XMLHttpRequest();
            var requestSrc =  $('.js-gallery-page-load').attr('data-json');
            jsonResult.open("GET", requestSrc);
            jsonResult.responseType = 'json';
            jsonResult.send();
            jsonResult.onload = function(e) {
                listResult = jsonResult.response
                var list = listResult.gallery
                if(list.length < numberLoad){
                    $('.js-gallery-page-load').addClass('stop')
                }
                numberLoad += 3
                var typeClass,
                    sizeClass,
                    desc,
                    descTitle
                $.each(list, function(key, val) {
                    if(key < numberLoad && key >= numberLoad - 3) {
                        if(val.galleryType == "photo"){
                            typeClass = 'gallery-page__item--photo'
                        }else {
                            typeClass = 'gallery-page__item--video'
                        }
                        if(val.gallerySize == "w2"){
                            sizeClass = 'gallery-page__item--w2'
                        }else {
                            sizeClass = ""
                        }
                        if(val.galleryDesc != undefined) {
                            desc = `<p class="gallery-page__desc">${val.galleryDesc}</p>`
                            descTitle = val.galleryDesc
                        }else {
                            desc = ""
                            descTitle = ""
                        }
                        $('.gallery-page__item').last().after(`
                            <a href="#" class="gallery-page__item animated animated--type-2 ${typeClass} ${sizeClass}" data-title="${descTitle}" data-src="${val.galleryLink}" data-fancybox="group-1" data-filter="${val.galleryFilter}">
                                <img src="${val.galleryImg}" alt="">      
                                ${desc}                          
                            </a>
                        `)
                    }
                })              
                $('.filter__link._current').each(function(){
                    let link = $(this).data('link')
                    if(link != 'all'){
                        $('[data-filter]').each(function(){
                            var tags = $(this).attr('data-filter');
                            if(!tags.includes(link)) {
                                $(this).addClass('_hide')
                            }else {
                                $(this).removeClass('_hide')
                            }
                        })
                    }  
                })
                if($('.js-massonry').length){
                    $('.js-massonry').each(function () {
                        $(this).masonry();
                    });
                    // $('[data-fancybox]').fancybox().update();
                }
            } 
        }
    }
}
if($('.js-books-page-load').length) {
    let numberLoad = 0
    $(document).on('scroll', function(){
        $loadbox('.js-click-load-books-page', loadBook)
    })
    $(document).on('click', '.js-click-load-books-page', function(){
        loadBook()
        return false
    })
    function loadBook(){
        if($(window).scrollTop() + $(window).height() >= $('.js-books-page-load').height() && !$('.js-books-page-load').hasClass('stop')) {
            var listResult
            var jsonResult = new XMLHttpRequest();
            var requestSrc =  $('.js-books-page-load').attr('data-json');
            jsonResult.open("GET", requestSrc);
            jsonResult.responseType = 'json';
            jsonResult.send();
            jsonResult.onload = function(e) {
                listResult = jsonResult.response
                var list = listResult.books
                if(list.length < numberLoad){
                    $('.js-books-page-load').addClass('stop')
                }
                numberLoad += 3
                $.each(list, function(key, val) {
                    if(key < numberLoad && key >= numberLoad - 3) {
                        
                        $('.books-page__item').last().after(`
                            <div class="books-page__item  books-page__item--type-2 animated animated--type-2" data-filter="${val.booksTags}">
                                <a href="${val.booksLink}" class="books-page__item_img-box js-parallax js-parallax-no">
                                    <img src="${val.booksImg}" alt="">
                                </a>
                                <div class="books-page__item_text">
                                    <div class="books-page__item_text_container">
                                        <div class="books-page__item_info">
                                            <span>${val.booksDate}</span>
                                            <span>${val.booksAuthorship}</span>
                                        </div>
                                        <a href="${val.booksLink}">${val.booksTitle}</a>
                                        <p>${val.booksText}</p>
                                        <a href="${val.booksLinkShop}" class="books-page__item_shop" target="_blank">
                                            <span>В магазин</span>
                                            <svg>
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-bt"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        `)
                    }
                })
                $('.js-parallax-no').each(function(){
                    $(this).hover3d({
                        selector: "img",
                        shine: false,
                    });   
                    $(this).removeClass('js-parallax-no')   
                })
                $('.filter__link._current').each(function(){
                    let link = $(this).data('link')
                    if(link != 'all'){
                        $('[data-filter]').each(function(){
                            var tags = $(this).attr('data-filter');
                            if(!tags.includes(link)) {
                                $(this).addClass('_hide')
                            }else {
                                $(this).removeClass('_hide')
                            }
                        })
                    }  
                })
            } 
        }
    }
}
function $loadbox(el, fn) {
	var windowHeight = $(document).scrollTop() + $(window).height();
    var $this = $(el),
        height = $this.offset().top + $this.height();
    if(windowHeight >= height) {
        fn()
    }
}