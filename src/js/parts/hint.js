$('.hint').on('mousemove',  function(e){
    var target = e.target;
    var targetCoords = target.getBoundingClientRect();
    var yCoord = e.clientY - targetCoords.top;
    var xCoord = e.clientX - targetCoords.left;
    moveCursor($(this), xCoord, yCoord, .5)
    $(this).addClass('_hover')   
});

$('.hint').on('mouseout', function(e){
    $(this).removeClass('_hover')
});

function hintPosition(el) {
    let widthPage = $(window).width()
    let elPosition = $(el).offset().left
    if(elPosition < 150){
        el.addClass('_left')
    }else {
        el.removeClass('_left')
    }
    if(elPosition > widthPage - 150){
        el.addClass('_right')
    }else {
        el.removeClass('_right')
    }
}
$('.hint').each(function(){
    hintPosition($(this))
})
$(window).resize(function(){
        $('.hint').each(function(){
        hintPosition($(this))
    })
})
$(document).on('click', '.hint', function(){
    $(this).toggleClass('_open')
})
$(document).on('click', '.hint__close', function(){
    $(this).toggleClass('_open')
})