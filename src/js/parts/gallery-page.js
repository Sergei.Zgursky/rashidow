$('.gallery-page__item').each(function(){
  if($(this).find('.gallery-page__desc').length){
    $(this).attr('data-title', $(this).find('.gallery-page__desc').text())
  }
})

function $tile($el) {
    if (!$el.length) return;
      $el.masonry({
          itemSelector: '.gallery-page__item',
          singleMode: true,
          isResizable: true,
          isAnimated: true,
          columnWidth: '.gallery-page__width',
          animationOptions: {
              queue: false,
              duration: 500
          }
      });
};
function masonryUpdate() {
  $('.js-massonry').each(function () {
    var $masonry = $(this);
    var update = function () {
        $masonry.masonry('reloadItems')
        $masonry.masonry();
      };
      $('.gallery-page__item:not(._hide)', $masonry);
      this.addEventListener('load', update, true);
      $masonry.masonry();
      // $masonry.masonry('destroy')
  });
}
function mas() {
  $('.js-massonry').each(function () {
  var $masonry = $(this);
    $masonry.masonry('destroy')
  })
}
$(window).bind('load', function() {
  masonryUpdate()
})