
function moveCursor($cursor, coordX, coordY, time) {
    $cursor.addClass('is-moving');

    gsap.to($cursor, time, {
        '--left': coordX+'px',
        '--top': coordY+'px',
        ease: Power4.easOut
    });
}
$(document).on('mousemove', '.gallery-page__item', function(e){
    moveCursor($(this), e.offsetX, e.offsetY, .5)
    $(this).addClass('_hover')
});
$(document).on('mouseout', '.gallery-page__item', function(e){
    $(this).removeClass('_hover')
});
$('.js-cursor').on('mousemove',  function(e){

    if(!e.target.closest('.biography__main_tabs_slider_bt') && !e.target.closest('.museum-page__slider_bt')){
        var target = e.target.closest('.js-cursor');
        var targetCoords = target.getBoundingClientRect();
        var yCoord = e.clientY - targetCoords.top;
        var xCoord = e.clientX - targetCoords.left;
        moveCursor($(this), xCoord, yCoord, .5)
        $(this).addClass('_hover')   
    }else {
        $(this).removeClass('_hover') 
    }
});
$('.js-cursor').on('mouseout', function(e){
    $(this).removeClass('_hover')
});