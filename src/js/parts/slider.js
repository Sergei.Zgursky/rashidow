var sliderGallery = new Swiper('.gallery__slider_box', {
    spaceBetween: 0,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
        renderFraction: function (currentClass, totalClass, index, total) {
            return '<span class="' + currentClass + '">'+ index +' </span>' +
                '<i class="gallery__slider_separation"></i>'+
                '<span class="' + totalClass + '">'+ total +' </span>';
        },
    },
    navigation: {
        nextEl: '.gallery__slider_bt_next',
        prevEl: '.gallery__slider_bt_prev',
    },
});

var sliderArticle = new Swiper('.article__slider_box', {
    spaceBetween: 0,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
        renderFraction: function (currentClass, totalClass, index, total) {
            return '<span class="' + currentClass + '">'+ index +' </span>' +
                '<i class="gallery__slider_separation"></i>'+
                '<span class="' + totalClass + '">'+ total +' </span>';
        },
    },
    navigation: {
        nextEl: '.article__slider_bt_next',
        prevEl: '.article__slider_bt_prev',
    },
});
var sliderBooks
function sliderBooksSl() {
    sliderBooks = new Swiper('.books-page__container', {
        spaceBetween: 22,
        slidesPerView: 'auto',
        centeredSlides: true,
        loop: $('.books-page__container .books-page__item:not(._hide )').length > 3 ? true:false,
        loopedSlides: 5,
        initialSlide: 1,
        navigation: {
            nextEl: '.books-page__slider_bt_next',
            prevEl: '.books-page__slider_bt_prev',
        },
        breakpoints: {
            1830: {
                centeredSlides: true,
                initialSlide: 1,
            },
            0: {
                centeredSlides: false,
                initialSlide: 0, 
            },
        }
    });
}
sliderBooksSl()

var sliderMuseum = new Swiper('.museum-page__slider_container', {
    spaceBetween: 0,
    slidesPerView: 5,
    navigation: {
        nextEl: '.museum-page__slider_bt_next',
        prevEl: '.museum-page__slider_bt_prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 5,
        },
        767: {
            slidesPerView: 4,
        },
        480: {
            slidesPerView: 3,
        },
        0: {
            slidesPerView: 2,
        },
    }
});

var sliderGallery = new Swiper('.museum-detail__text-box_slider_container', {
    spaceBetween: 0,
    effect: 'fade',
    on: {
        init: function () {
            setTimeout(()=>{
                $('.museum-detail__text-box_slider_slide img').eq(0).on('load', function() {
                    slideHeight(0)
                })
            },100)
        },
    },
    navigation: {
        nextEl: '.museum-detail__text-box_slider_bt_next',
        prevEl: '.museum-detail__text-box_slider_bt_prev',
    },
});
function slideHeight(i) {
    let topBt = $('.museum-detail__text-box_slider_slide').eq(i).find('img').height()
    $('.museum-detail__text-box_slider').css('--top', topBt)
}
sliderGallery.on('slideChange', function (e) {
    slideHeight(this.activeIndex);
});

window.addEventListener('resize', () => {
    slideHeight(sliderGallery.realIndex);
});
var speedBiography = 39000
var sliderBiography = new Swiper('.biography__slider_container', {
    loop: true,
    direction: 'vertical',    
    scrollContainer: false,
    mousewheelControl: false,
    autoHeight: false,
    speed: speedBiography,
    autoplay: {
        enabled: true,
        delay: 1,
        disableOnInteraction: false,
        waitForTransition: false
    },
    breakpoints: {
        767: {
            slidesPerView: 1.3,
            spaceBetween: 47,
        },
        480: {
            slidesPerView: 1.4,
            spaceBetween: 17,
        },
        0: {
            slidesPerView: 3,
            spaceBetween: 17,
        },
    }
});
var sliderBiographyRev = new Swiper('.biography__slider_container--reverse', {
    loop: true,
    direction: 'vertical',    
    scrollContainer: false,
    mousewheelControl: false,
    autoHeight: false,
    speed: speedBiography,
    autoplay: {
        enabled: true,
        delay: 1,
        disableOnInteraction: false,
        waitForTransition: true,
        reverseDirection: true,
    },
    breakpoints: {
        767: {
            slidesPerView: 1.3,
            spaceBetween: 47,
        },
        480: {
            slidesPerView: 1.4,
            spaceBetween: 17,
        },
        0: {
            slidesPerView: 3,
            spaceBetween: 17,
        },
    }
});

var sliderBiographyMain = new Swiper('.biography__main_slider_container', {
    spaceBetween: 0,
    centeredSlides: true,
    slideToClickedSlide: true,
    speed: 700,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: '.biography__main_slider_bt_next',
        prevEl: '.biography__main_slider_bt_prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 2.3,
        },
        767: {
            slidesPerView: 2.1,
        },
        0: {
            slidesPerView: 1, 
        },
    }
});
$('.biography__main_slider_slide').eq(1).each(function(i){
    let chapterClosest = $(this).find('.biography__main_slider_chapter').html()
    $(this).closest('.biography__main_slider').attr('data-next', chapterClosest)

})
var slideSum = $('.biography__main_slider_slide').length
sliderBiographyMain.on('slideChange', function(e) {
    let eqSlide = sliderBiographyMain.activeIndex +1
    let chapterClosestNext = $('.biography__main_slider_slide').eq(eqSlide).find('.biography__main_slider_chapter').html()
    let chapterClosestPrev = $('.biography__main_slider_slide').eq(eqSlide - 2).find('.biography__main_slider_chapter').html()
    if(eqSlide != 1){
        $('.biography__main_slider').attr('data-prev', chapterClosestPrev)
    }else {
        $('.biography__main_slider').removeAttr('data-prev')
    }
    if(slideSum != eqSlide){
        $('.biography__main_slider').attr('data-next', chapterClosestNext)
    }else {
        $('.biography__main_slider').removeAttr('data-next')
    }
    sliderBiographyMainBg.slideTo(sliderBiographyMain.activeIndex, 1000, false);  
    $('.biography__main_tab').eq(sliderBiographyMain.activeIndex).addClass('_current').siblings('.biography__main_tab').removeClass('_current')
    sliderBiographyMainTabs.destroy()
    slBiogTabs(document.querySelectorAll('.biography__main_tab')[sliderBiographyMain.activeIndex].querySelector('.biography__main_tabs_slider_container'))
});  
var sliderBiographyMainBg = new Swiper('.biography__main_slider_bg_container', {
    spaceBetween: 0,
    slidesPerView: 1,
    speed: 700,
    effect: 'fade',
    thumbs: {
      swiper: sliderBiographyMain
    }
});
sliderBiographyMainBg.on('slideChange', function(e) {
    sliderBiographyMain.slideTo(sliderBiographyMainBg.activeIndex, 1000, false)
})
var sliderBiographyMainTabs;
function slBiogTabs(el){
    sliderBiographyMainTabs = new Swiper(el, {
        spaceBetween: 0,
        slidesPerView: 5,
        navigation: {
            nextEl: '.biography__main_tabs_slider_bt_next',
            prevEl: '.biography__main_tabs_slider_bt_prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 5,
            },
            767: {
                slidesPerView: 4,
            },
            480: {
                slidesPerView: 3,
            },
            0: {
                slidesPerView: 2,
            },
        }
    });
}
$('._current .biography__main_tabs_slider_container').each(function(){
    slBiogTabs(this)
})
