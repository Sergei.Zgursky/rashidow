if($('.totop').length){
  var scrollToTopBtn= document.querySelector(".totop")
  var rootElement = document.documentElement
  var winHeight = document.querySelector(".totop")
  function handleScroll() {
    var scrollTotal = rootElement.scrollHeight - rootElement.clientHeight
    if ((rootElement.scrollTop / scrollTotal ) > 0.80) {
      scrollToTopBtn.style.display = "block"
    } else {
      scrollToTopBtn.style.display = "none"
    }
  }
  
  function scrollToTop() {
    rootElement.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }
  
  scrollToTopBtn.addEventListener("click", function(e){
    if(document.querySelectorAll(".totop").length && document.body.clientHeight  > window.innerHeight  * 2 ) {
        e.preventDefault()
        scrollToTop()
      }
  })
  document.addEventListener("scroll", handleScroll)
}