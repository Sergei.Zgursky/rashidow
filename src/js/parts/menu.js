function menuMob() {
    document.querySelector('.header__wrapper').insertAdjacentHTML('beforeEnd', `<div class="header__menu-mobile"><a href="#" class="header__menu-mobile_btn"><i></i><i></i><i></i><i></i></a><div class="header__menu-mobile_box"></div></div>`)
    let cloneMenuB = document.querySelector('.header__menu').cloneNode(true)
    let cloneLang = document.querySelector('.header__lang').cloneNode(true)
    // .appendChild(document.querySelector('.header__menu-mobile_box'))
    document.querySelector('.header__menu-mobile_box').appendChild(cloneLang)
    document.querySelector('.header__menu-mobile_box').appendChild(cloneMenuB)
    if(document.querySelectorAll('.btn-alternative').length){
        let cloneAlternative = document.querySelector('.btn-alternative').cloneNode(true)
        document.querySelector('.header__menu-mobile_box').appendChild(cloneAlternative)
    }
}
menuMob()
document.querySelector('.header__menu-mobile_btn').addEventListener('click', (e) => {
    e.stopPropagation()
    e.target.classList.toggle('_active')
    e.target.closest('.header__menu-mobile').classList.toggle('_active')
})