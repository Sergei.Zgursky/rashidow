if($('.filter').length){
    $('.filter').on('click', '.filter__link', function(){
        let link = $(this).data('link')
        $(this).addClass('_current').siblings('.filter__link').removeClass('_current')
        sessionStorage.setItem('filter', link);
        if(link != 'all'){
            $('[data-filter]').each(function(){
                var tags = $(this).attr('data-filter');
                if(!tags.includes(link)) {
                    $(this).addClass('_hide')
                }else {
                    $(this).removeClass('_hide')
                }
            })
        }else {
            $('._hide[data-filter]').removeClass('_hide')
        }
        if($('.js-massonry').length){
            $('.js-massonry').each(function () {
               $(this).masonry();
            });
        }
        if(('.books-page__container').length){
            sliderBooks.destroy()
            sliderBooksSl()
        }
        return false
    })
}