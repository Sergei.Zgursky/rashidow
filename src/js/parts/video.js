$.findVideos = function () {
    let videos = document.querySelectorAll('.videoBox__wrapper');

    for (let i = 0; i < videos.length; i++) {
        setupVideo(videos[i]);
    }
};
function setupVideo(video) {
	let link = video.querySelector('.video__link');
	let media = video.querySelector('.video__media');
	let id = parseMediaURL(link);
	video.addEventListener('click', function(e){
        e.preventDefault()
		let iframe = createIframe(id);

		link.remove();
        
		video.appendChild(iframe);
	});

	video.classList.add('video--enabled');
};

function parseMediaURL(link) {
	let regexp = /https:\/\/img\.youtube\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
	let url = link.getAttribute('href');
	let match = url.match(regexp);
    console.log(url);
	return url;
	// link.removeAttribute('href');
};

function createIframe(id) {
	let iframe = document.createElement('iframe');

	iframe.setAttribute('allowfullscreen', '');
	iframe.setAttribute('allow', 'autoplay');
	iframe.setAttribute('frameborder', '0');
	iframe.setAttribute('src', generateURL(id));
	iframe.classList.add('video__media');

	return iframe;
};

function generateURL(id) {
	let query = '?rel=0&showinfo=0&autoplay=1';

	return  id + query;
};
$.findVideos()