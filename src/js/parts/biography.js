$(document).on('click', '.js-biography__scroll', function () {
    $('.biography__first').addClass('_hide')
    $('body').addClass('_second')
    return false
})
$(window).on('wheel', function () {
    if (!$('.biography__first').hasClass('_hide')) {
        $('.biography__first').addClass('_hide')
        $('body').addClass('_second')
    }
})
var swipe = (function (area) {
    var ctrl = {
        area: undefined,
        touchstarty: undefined,
        touchmovey: undefined,
        movey: undefined,
        init: function (area) {
            ctrl.area = area;
            ctrl.actions();
        },
        actions: function () {
            $(ctrl.area).on("touchstart", function (event) {
                ctrl.start(event);
            });
            $(ctrl.area).on("touchmove", function (event) {
                ctrl.move(event);
            });
            $(ctrl.area).on("touchend", function (event) {
                ctrl.end(event);
            });
        },
        start: function (event) {
            ctrl.touchstarty = event.originalEvent.touches[0].pageY;
            ctrl.touchmovey = 0;
        },
        move: function (event) {
            ctrl.touchmovex = event.originalEvent.touches[0].pageY;
            ctrl.movey = ctrl.touchstarty - ctrl.touchmovey;
        },
        end: function (event) {
            if (ctrl.touchmovey !== 0) {
                if (ctrl.movey < 50) {
                    ctrl.top();
                }
                if (ctrl.movey > -50) {
                    ctrl.bottom(ctrl.area);
                }
            }
        },
        
    };
    ctrl.init(area);
    return ctrl;
});
var menuSwipe = swipe(document);
menuSwipe.move = function (event) {
    menuSwipe.touchmovey = event.originalEvent.touches[0].pageY;
    menuSwipe.movey = menuSwipe.touchstarty - menuSwipe.touchmovey;
}
menuSwipe.bottom = function () {
    if (!$('.biography__first').hasClass('_hide')) {
        $('.biography__first').addClass('_hide')
        $('body').addClass('_second')
    }
}
menuSwipe.top = function () {}
$('.biography-detail__menu').clone().addClass('_mob').removeClass('animated').removeClass('animated--type-2').appendTo('body')
if($('.biography-detail__menu:not(.biography-detail__menu--type-2)').length){
    $('body').addClass('paddingMenu')
}
$(document).on('click', '.biography-detail__menu_open', function(){
    $(this).closest('.biography-detail__menu').toggleClass('_open')
    return false
})

var listLinkTag = []
$('.biography-detail__menu_tag').each(function(){
    var linkTag = $(this).data('teg')
    if($(this).hasClass('_active')){
        listLinkTag.push(linkTag);
    }
})
$(document).on('click', '.biography-detail__menu_tag', function(){
    $(this).toggleClass('_active')
    var linkTag = $(this).data('teg')
    if($(this).hasClass('_active')){
        listLinkTag.push(linkTag);
    }else {
        listLinkTag.pop(linkTag);
        if($(this).hasClass('_active')){
            listLinkTag.push(linkTag);
        }
    }
    let listLinkTagStr = listLinkTag.join('&tag=')
    if($('.biography-detail__menu_tag._active').length){
        if($('.biography-detail__menu_tags_box_bt_apply').attr('href').indexOf('tag=')>0){
            let link = $('.biography-detail__menu_tags_box_bt_apply').attr('href').split('?')[0];
            let newLink = link + '?tag=' + listLinkTagStr
            $('.biography-detail__menu_tags_box_bt_apply').attr('href', newLink)
        }else {
            let newLink = $('.biography-detail__menu_tags_box_bt_apply').attr('href') + '?tag=' + listLinkTagStr
            $('.biography-detail__menu_tags_box_bt_apply').attr('href', newLink)
            
        }
    }else {
        let link = $('.biography-detail__menu_tags_box_bt_apply').attr('href').split('?')[0];
        $('.biography-detail__menu_tags_box_bt_apply').attr('href', link)
    }
    return false
})
if($('.biography-detail__content--page').length){
    let number = $('.biography-detail__menu_nav_link_number').html()
    let chapter = $('.biography-detail__menu_nav_link_name').html()
    let section = $('.biography-detail__menu_nav_chapter_link._current').html()
    $('.biography-detail__content--page .link-back').after(`
        <div class="biography-detail__content_title">
            <span class="biography-detail__content_title_number">${number}</span>
            <span class="biography-detail__content_title_chapter">${chapter}:${section}</span>
        </div>
    `);
}