if(document.querySelectorAll('.js-scroll-first').length) {
  var btnScrollDown = document.querySelector('.js-scroll-first');
  
    function scrollDown() {
      var windowCoords = document.documentElement.clientHeight;
      (function scroll() {
        if (window.pageYOffset < windowCoords) {
          window.scrollBy(0, 10);
          setTimeout(scroll, 0);
        }
        if (window.pageYOffset > windowCoords) {
          window.scrollTo(0, windowCoords);
        }
      })();
    }
  
  btnScrollDown.addEventListener('click', function(e){
    e.stopPropagation()
    e.preventDefault() 
    scrollDown()
  });

}

if($('.books').length && $('.museum__bg').length) {
  function museumBg(){
    let topBox = $('.books').offset().top
    if(topBox < window.scrollY) {
      $('.museum__bg').addClass('_hide')
    }else{
      $('.museum__bg').removeClass('_hide')
  
    }
  }
  $(window).on('scroll', function(){
    museumBg()
  })
  museumBg()
}