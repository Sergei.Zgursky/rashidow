if($('.main-about__gallery').length) {
    let time = $('.main-about__gallery_slide').length * 150
    $('.main-about__gallery').css('--time', `${time}s`)
    $('.main-about__gallery_slide').each(function(){
        $(this).clone().appendTo('.main-about__gallery_wrapper')
    })
}