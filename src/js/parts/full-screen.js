function fullScreen(){
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}
fullScreen()
var mql = window.matchMedia("screen and (min-width: 1025px)");
window.addEventListener('resize', () => {
    if(!mql.matches){
        fullScreen()
    }
});
window.addEventListener('orientationchange', () => {
   fullScreen()
});
