$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});
//popup
$(document).on('click', '[data-modal="text"]', function(e){

    e.preventDefault();
    var src = $(this).attr('href'), 
        bt = $(this);
    $.fancybox.open({
        src: src,
        type: 'inline',
        hash: false,
        opts: {
            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small close" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/html/img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#popup__search-main') {
                    $('.fancybox-slide').addClass('event-no')
                }
            },
            afterShow: function(instance, current) {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                if (current.src == '#popup__search-main') {
                    if(!$(current.src).find('select').hasClass('complide')){
                        $('.select').each(function(){
                            $.select($(this));
                        });
                        $('.multiple').each(function(){
                            $.selectMulte($(this));
                        });
                        $(current.src).find('select').addClass('complide')
                    }
                    if($('.datepicker__check').length != 2){
                        $('.datepicker:eq(1)').find('.datepicker--content').after('<div class="datepicker__check"><label><input type="checkbox"><span>Гибкие даты (+/-10 дн.)</span></label></div>'); 
                    }
                    $('body').addClass('_fixed');
                }
                
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            },
            afterClose: function(instance, current) {
                $('body').removeClass('_fixed');
                if (current.src == '#popup__search-main') {
                  
                }
            },
        }
    });    
});

$.fancyOptions = {
    type: 'image',
    buttons: [
        "smallBtn"
    ],
    thumbs: {
        autoStart: true,
        axis: 'x',
        hideOnClose: true,    
        parentEl: '.fancybox-container',  
    },
    afterShow: function() {
        $('.Valid').each(function(){
            $.Valid($(this));
        });
    },
    hash: false,
    loop: true,
    btnTpl: {
       smallBtn: '<button data-fancybox-close class="fancybox-close-small-img" title="{{CLOSE}}">' +
                    '<span>Закрыть</span>' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/html/img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',
        arrowLeft:
          '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left">' +
                 '<svg style="width: 1.19rem; height: 0.81rem; transform: rotate(180deg);">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrowmod"></use>' +
                 '</svg>' +
              '</button>',

        arrowRight:
          '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right">' +
                 '<svg style="width: 1.19rem; height: 0.81rem;">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrowmod"></use>' +
                 '</svg>' +
            '</button>'
    },
    lang: "ru",
    i18n: {
        ru: {
          CLOSE: "Закрыть",
          NEXT: "Вперёд",
          PREV: "Назад",
          ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
          PLAY_START: "Запустить слайдшоу",
          PLAY_STOP: "Остановить слайдшоу",
          FULL_SCREEN: "На весь экран",
          THUMBS: "Миниатюры",
          DOWNLOAD: "Скачать",
          SHARE: "Поделиться",
          ZOOM: "Увеличить"
        }
    },
    video: {
        autoStart: true
    },
    media : {
        youtube : {
            params : {
                autoplay : 1
            }
        }
    },
    afterLoad: function(instance, current) {
        current.$content.closest('.fancybox-container').addClass('photo');
    }
}
$.fancy = function (el) {

    var $el = el;

    if (!$el.length) return;

    $el.attr('data-init', 'true');

    $el.fancybox($.fancyOptions);

};
$().fancybox({
    selector: '[data-fancybox*="group"]:not(._hide)',
    buttons: [
        "smallBtn"
    ],
    animationEffect: "zoom-in-out",
    width : 1108,
    height : 656,
    afterShow: function() {
        $('.Valid').each(function(){
            $.Valid($(this));
        });
    },
    // hash: false,
    loop: true,
    caption : function( instance, item ) {
        var caption = $(this).data('title') || '';
            caption = String(caption)
        if ( item.type === 'image' ) {
            caption = (caption.length ? caption: '') + ' ' ;
        }

        return caption;
    },
    btnTpl: {
       smallBtn: '<button data-fancybox-close class="fancybox-close-small-img" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',
        arrowLeft:
          '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left">' +
                 '<svg>' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-left"></use>' +
                 '</svg>' +
              '</button>',

        arrowRight:
          '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right">' +
                 '<svg>' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow-left"></use>' +
                 '</svg>' +
            '</button>'
    },
    lang: "ru",
    i18n: {
        ru: {
          CLOSE: "Закрыть",
          NEXT: "Вперёд",
          PREV: "Назад",
          ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
          PLAY_START: "Запустить слайдшоу",
          PLAY_STOP: "Остановить слайдшоу",
          FULL_SCREEN: "На весь экран",
          THUMBS: "Миниатюры",
          DOWNLOAD: "Скачать",
          SHARE: "Поделиться",
          ZOOM: "Увеличить"
        }
    },
    afterLoad: function(instance, current) {
        current.$content.closest('.fancybox-container').addClass('photo');
    }
});