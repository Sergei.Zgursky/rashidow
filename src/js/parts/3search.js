if($('.search').length) {
    let linkPage = window.location.href 
    if(linkPage.indexOf('tag=')!=-1){
        linkPage = linkPage.split('?')[1].replace(/tag=/g, '')
        linkPage=linkPage.split('&') 
        loadSearch(linkPage)
        let tagList = linkPage
        for (let i = 0; i < tagList.length; i++) {
                $('[data-teg="'+tagList[i]+'"]').addClass('_active')
        }
    }else {
        let tags = []
        loadSearch(tags)
    }
    let numberLoad = 0
    function loadSearch(tags){
        if($(window).scrollTop() + $(window).height() >= $('.search').height() && !$('.search').hasClass('stop')) {
            var listResult
            var jsonResult = new XMLHttpRequest();
            var requestSrc =  $('.search').attr('data-json');
            jsonResult.open("GET", requestSrc);
            jsonResult.responseType = 'json';
            jsonResult.send();
            jsonResult.onload = function(e) {
                listResult = jsonResult.response
                var list = listResult.search
                $.each(list, function(key, val) {
                    if(tags.length > 0){
                        let arr = val.searchFilter.split(',') 
                        for (let i = 0; i < arr.length; i++) {
                            if(tags.join(' ').indexOf(arr[i].trim()) != -1) {
                                $('.search').append(`
                                    <div class="search__item" data-teg="${val.searchFilter}">
                                    <div class="search__tags-list"></div>
                                    <a href="${val.searchLink}" class="search__name">${val.searchTitle}</div>
                                    </div>
                                `)
                                let arr = val.searchTags.split(',') 
                                for (let i = 0; i < arr.length; i++) {
                                    $('.search__item').last().find('.search__tags-list').append(
                                        `<div class="search__tag">${arr[i]}</div>`
                                    )
                                }
                            }                            
                        }
                    }else {
                        $('.search').append(`
                            <div class="search__item" data-teg="${val.searchFilter}">
                            <div class="search__tags-list"></div>
                            <a href="${val.searchLink}" class="search__name">${val.searchTitle}</div>
                            </div>
                        `)
                        let arr = val.searchTags.split(',') 
                        for (let i = 0; i < arr.length; i++) {
                            $('.search__item').last().find('.search__tags-list').append(
                                `<div class="search__tag">${arr[i]}</div>`
                            )
                        }
                    }
                })
            } 
        }
    }
}