$(document).on('click', '.tabs__link:not(._current)', function() {
    $(this).addClass('_current').siblings().removeClass('_current')
        .parents('.tabs').find('.tabs__box ').eq($(this).index()).fadeIn().siblings('.tabs__box').hide();
    return false
});