
var wow = new WOW({
    boxClass:     'animated',      
    animateClass: 'on',
    offset:       0,
    mobile:       true,   
    live:         true,    
    callback:     function(box) {
    },
    scrollContainer: null 
});
wow.init();