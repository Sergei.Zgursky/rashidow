
    'use strict';
if($('.no-touchevents').length) {
    var $slider = $('.horizonatl-scroll'),
        $slides = $('.museum__line'),
        $sliderWrapper = $('.horizonatl-scroll__wrapper'),
        $sliderTitle = $('.museum__title-box_containeer'),
        $firstSlide = $slides.first();

    var settings = {},
        resizing = false,
        scrollController = null,
        scrollTween = null,
        scrollTimeline = null,
        progress = 0,
        scrollScene = null,
        widthSl = $('.museum__wrapper').width() - $(window).width();

    function update(){
      $slider = $('.horizonatl-scroll'),
      $slides = $('.museum__line'),
      $sliderWrapper = $('.horizonatl-scroll__wrapper'),
      $sliderTitle = $('.museum__title-box_containeer'),
      $firstSlide = $slides.first(),
      settings = {},
      resizing = false,
      scrollController = null,
      scrollTween = null,
      scrollTimeline = null,
      progress = 0,
      scrollScene = null,
      widthSl = $('.museum__wrapper').width() - $(window).width();
      scrollSlider()
    }
    function scrollSlider(options) {

        settings = $.extend({
            slider: '.horizonatl-scroll',
            sliderWrapper: '.horizonatl-scroll__wrapper',
            slides: '.museum__line',
            slideWidth: null,
            slideHeight: null,
        }, options);

        setDimensions();
        
        $(window).on( 'resize', function() {
          clearTimeout(resizing);
          resizing = setTimeout(function() {
            setDimensions();
          }, 250); 
        });
      
    }

    function setDimensions() {

        settings.slideWidth = $firstSlide.width();
        settings.slideHeight = $firstSlide.height();
            
        // Calculate slider width and height
        settings.sliderWidth =  widthSl;
        settings.sliderHeight = $firstSlide.outerHeight(true);
        // Set slider width and height
        $sliderWrapper.width(settings.sliderWidth);
        //$sliderWrapper.height(settings.sliderHeight);

        // Set scene
        setScene();

        //resizing = false;
    }
    function setScene() {

      var xDist = -widthSl,
      tlParams = { x: xDist, ease: Linear.easeInOut };
      tlParamsMinus = { x: -xDist, ease: Linear.easeNone };
      if (scrollScene != null && scrollTimeline != null) {
          
          progress = 0;
          scrollScene.progress(progress);

          scrollTimeline = new TimelineMax();
          scrollTimeline
            .to( $sliderWrapper, 2, tlParams )
            .to( $sliderTitle, 2, tlParamsMinus, 0 );
        
          scrollScene.setTween(scrollTimeline);
        
          scrollScene.refresh();
          
        } else {
          // Init ScrollMagic controller
          scrollController = new ScrollMagic.Controller();
          
          //Create Tween
          scrollTimeline = new TimelineMax();
          scrollTimeline
            .to( $sliderWrapper, 2, tlParams )     
            .to( $sliderTitle, 2, tlParamsMinus, 0 );
          scrollTimeline.progress( progress );
          
        
          // Create scene to pin and link animation
          scrollScene = new ScrollMagic.Scene({
              triggerElement: settings.slider,
              triggerHook: "onLeave",
              duration: settings.sliderWidth
          })
          .setPin(settings.slider)
          .setTween(scrollTimeline)
          .addTo(scrollController)
          .on('start', function (event) {
            scrollTimeline.time(0);
          });
      }
        
    }
    

  $('.museum__wrapper').css('--width', $('.museum__wrapper').width()+'px' )
  function startScroll(mql) {
    if (mql.matches) {
      if($('.scrollmagic-pin-spacer').length) {
        scrollScene.destroy(true)
      }
      $('.horizonatl-scroll__box').addClass('_scroll')
      
    } else {
      if(!$('.scrollmagic-pin-spacer').length) {
        update()
      }
      $('.horizonatl-scroll__box').removeClass('_scroll')
    }
  }  
  var mql = window.matchMedia("screen and (max-width: 1024px)");

  mql.addListener(startScroll); 

  startScroll(mql); 
}else {
  $('.horizonatl-scroll__box').addClass('_scroll')
}
