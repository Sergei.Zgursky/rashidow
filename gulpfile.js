'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    webp = require('imagemin-webp'),
    extReplace = require("gulp-ext-replace"),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    rename = require('gulp-rename'),
    order = require('gulp-order'),
    concat = require('gulp-concat'),
    browserSync = require("browser-sync"),
    plumber = require('gulp-plumber'),
    include = require("gulp-include"),
    svgstore = require('gulp-svgstore'),
    svgmin = require('gulp-svgmin'),
    pathFnc = require('path'),
    webpcss = require("gulp-webp-css"),
    newer = require('gulp-newer'),
    changed = require('gulp-changed'),
    gcmq = require('gulp-group-css-media-queries'),
    reload = browserSync.reload,
    webpcssOptions = {
        webpClass: '',
        noWebpClass: '.no-webp'
    };

var path = {
    build: {
        html: '../public/',
        js: '../public/js/',
        json: '../public/json/',
        css: '../public/css/',
        img: '../public/img/',
        img_webp: 'src/media/img/**/*.{webp}',
        media: '../public/media/',
        fonts: '../public/fonts/'
    },
    src: {
        html: 'src/**/*.html',
        js: 'src/js/parts/*.js',
        js_vendor: 'src/js/vendors/*.js',
        js_back: 'src/js/back/*.js',
        js_libs: 'src/js/libs/*.js',
        json: 'src/json/**/*.json',
        style: 'src/style/**/*.scss',
        img: 'src/media/img/**/*.{jpg,jpeg,png,gif,svg,webp}',
        img_webp: 'src/media/img/**/*.{jpg,png}',
        media: ['src/media/audio/**/*.*', 'src/media/video/**/*.*'],
        svgIcons: 'src/media/img/sprite/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        json: 'src/json/**/*.json',
        style: 'src/style/**/*.scss',
        img: 'src/media/img/**/*.*',
        media: ['src/media/audio/**/*.*', 'src/media/video/**/*.*'],
        svgIcons: 'src/media/img/sprite/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: '../public'
};

var config = {
    server: {
        baseDir: "../public"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "TBaza"
};

gulp.task('webserver', function() {
    browserSync(config);
});

gulp.task('clean', function(cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function() {
    gulp.src(path.src.html)
        .pipe(include())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('js:build', function() {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(concat("scripts.js"))
        .pipe(gulp.dest(path.build.js))
        .pipe(uglify())
        .pipe(rename('scripts.min.js'))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({
            stream: true
        }))

    gulp.src(path.src.js_vendor)
        .pipe(plumber())
        .pipe(concat("vendors.js"))
        .pipe(gulp.dest(path.build.js))
        .pipe(uglify())
        .pipe(rename('vendors.min.js'))
        .pipe(gulp.dest(path.build.js));
		
    gulp.src(path.src.js_back)
        .pipe(plumber())
        .pipe(concat("back.js"))
        .pipe(gulp.dest(path.build.js));

    gulp.src(path.src.js_libs)
        .pipe(plumber())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('json:build', function() {
    gulp.src(path.src.json)
        .pipe(gulp.dest(path.build.json))
});

gulp.task('style:build', function() {
    gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(order([
            "vendors/**/*.scss",
            "mixin.scss",
            "settings.scss",
            "fonts.scss",
            "base.scss",
            "UI.scss",
            "layout/**/*.scss"
        ]))
        .pipe(sourcemaps.init())
        .pipe(concat("style.scss"))
        .pipe(sass({
            includePaths: ['src/style/style.scss'],
            outputStyle: 'expanded',
            sourceMap: false,
            errLogToConsole: true
        }))
        .pipe(prefixer({
            browsers: ['last 16 versions']
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(cssmin())
        .pipe(webpcss(webpcssOptions))
        .pipe(gcmq())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({
            stream: true
        }));

});

gulp.task('svg', function() {
    return gulp.src(path.src.svgIcons)
        .pipe(svgmin(function(file) {
            var prefix = pathFnc.basename(file.relative, pathFnc.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(rename({
            prefix: 'icon-'
        }))
        .pipe(svgstore())
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('image:build', function() {
    gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [
                pngquant({
                     quality: '100', 
                     speed: 1, 
                     floyd: 1
                }),
            ],
            interlaced: true,
            concurrent: 5,
            quality: '100', 
            speed: 1, 
            floyd: 1
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({
            stream: true
        }));
});
gulp.task("exportWebP", function() {
      gulp.src(path.src.img_webp)
        .pipe(newer(path.build.img_webp))
        .pipe(imagemin([
          webp({
              method: 6
          })
        ]))
        .pipe(rename({ extname: '.webp' }))
        .pipe(gulp.dest(path.build.img))
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'exportWebP',
    'html:build',
    'js:build',
    'json:build',
    'style:build',
    'fonts:build',
    'image:build',
    'svg'
]);

gulp.task('watch', function() {
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    //watch([path.watch.media], function(event, cb) {
    //   gulp.start('media:build');
    //});
    watch([path.watch.html], function(event, cb) {
        setTimeout(function() {
            gulp.start('html:build');
        }, 1000);
    });
    watch([path.watch.style], function(event, cb) {
        setTimeout(function() {
            gulp.start('style:build');
        }, 1000);
    });
    watch([path.watch.js], function(event, cb) {
        setTimeout(function() {
            gulp.start('js:build');
        }, 1000);
    });
    watch([path.watch.svgIcons], function(event, cb) {
        setTimeout(function() {
            gulp.start('svg');
        }, 1000);
    });
    watch([path.watch.json], function(event, cb) {
        setTimeout(function() {
            gulp.start('json:build');
        }, 1000);
    });
    watch([path.watch.fonts], function(event, cb) {
		setTimeout(function() {
            gulp.start('fonts:build');
        }, 1000);
    });
});

gulp.task('default', ['build', 'webserver', 'watch']);
gulp.task('prod', ['clean', 'build', 'watch']);
